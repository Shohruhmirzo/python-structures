"""
Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n,  0! = 1. n >= 0.
"""


def main():
    """Factorial calculation."""
    n = int(input())
    mul = 1
    if n == 0:
        print(1)
        return 0
        
    for i in range(1, n+1):
        mul = mul*i
    print(mul)

if __name__ == "__main__":
    main()
