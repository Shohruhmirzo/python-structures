"""
Consider a list (list = []). You can perform the following commands:
insert i e: Insert integer e at position i.
print: Print the list.
remove e: Delete the first occurrence of integer e.
append e: Insert integer e at the end of the list.
sort: Sort the list.
pop: Pop the last element from the list.
reverse: Reverse the list.

Initialize your list and read in the value of followed by lines of commands
where each command will be of the  types listed above. Iterate through each command
in order and perform the corresponding operation on your list.
The first line contains an integer, denoting the number of commands.
Each line  of the  subsequent lines contains one of the commands described above.
"""


def main():
    """Perform list commands."""
    n = int(input())
    l = []
    for _ in range(n):
        s=input()
        c,*v=s.split(); v=map(int,v)
        if c=='insert': l.insert(*v)
        elif c=='remove': l.remove(*v)
        elif c=='append': l.append(*v)
        elif c=='sort': l.sort()
        elif c=='reverse': l.reverse()
        elif c=='pop': l.pop(*v)
        elif c=='print': print(l)
        else: raise RuntimeError("invalid command")
        
if __name__ == "__main__":
    main()
