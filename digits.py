"""
Find sum of n-integer digits. n >= 0.
"""



def main():
    """Sum of number digits."""
    n = input()
    s = 0
    for i in n:
        s += int(i)
    print(s)
    
if __name__ == "__main__":
    main()
