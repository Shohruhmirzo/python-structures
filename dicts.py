"""
 Drop empty items from a dictionary.
"""
import json


def main():
    """Drop empty items from a dictionary."""
    d = json.loads(input())
    d = {k: v for k, v in d.items() if v}
    print(d)
    
if __name__ == "__main__":
    main()
